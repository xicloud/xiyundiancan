# 羲云点餐系统微信小程序多商户多门店版 含秒杀 满赠 优惠券等 微信原生

### 应用介绍
本小程序前端由羲云科技提供技术支持，可以连接到后台，进行数据的通讯。

### 接入
1,在小程序后端设置request域名 https://edit.zjhn.com
2,在小程序开发中，设置不校验域名。

### 功能介绍
1.秒杀
2.满赠
3.优惠券
4.积分
5.多门店
6.多商户
7.自行收款
.....
功能太多，具体请到官网查看

### 官网地址
https://www.zjhn.top
注册就可以使用最新版本应用

### 应用界面
![界面](https://www.zjhn.top/imgs/01.png)
![界面](https://www.zjhn.top/imgs/02.png)
![界面](https://www.zjhn.top/imgs/03.png)
![界面](https://www.zjhn.top/imgs/04.png)
![界面](https://www.zjhn.top/imgs/05.png)
![界面](https://www.zjhn.top/imgs/06.png)
![界面](https://www.zjhn.top/imgs/07.png)
![界面](https://www.zjhn.top/imgs/08.png)

由于应用开发原因，最新版本，请到官网了解更多。


