
var app = getApp()
var fun = require('../fun.js');
Page({
  data: {
    nav:{top:app.globalData.nav_top,title:'首页',home:'show',back:'show',safe_top:app.globalData.safe_top,bottom:app.globalData.safe_bottom,page:'home'},page:1,can_load:1,share_bt:'1'
  },

  onLoad: function (op) {
  },

  onReady: function () {
    this.load()
  },
load(){//加载
  var id=this.data.id;//是否加载门店
  //if(!id){shop_id=app.globalData.shop_id}//如果没有id,则选择默认
  if(!id){id=0;}
fun.get({ac:'get_index',path:'rest',id:id}).then(res=>{
this.setData({list:res.list,user:res.user,adv:res.adv});
if(res.app){
app.globalData.app=res.app;
//登录
if(res.app.data.login_first==1 && !res.user && res.app.data.index_login==1){
this.login();
}
}
if(res.shop){
  app.globalData.shop=res.shop;
  this.setData({shop:res.shop})
  }
})

},

  onShow: function () {

  },

  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
get_hb(){//分享海报
var shop=this.data.shop;
var shop_id=app.globalData.shop_id;
if(shop){shop_id=shop.id}
fun.get({ac:'get_shop_hb',id:shop_id,path:'hb'}).then(res=>{
this.setData({hb:res.hb,hb_show:'show',hb_index:0,share_box_show:''});
})
},hb_change(e){//切换海报后的index
  //console.log(e.detail)
var val=e.detail.current;
this.data.hb_index=val;
},
hb_save(e){//保存图片
  console.log('保存海报')
        var that=this;
        var index=this.data.hb_index
        var hb=this.data.hb;if(!hb){app.err('没有海报信息');return;}
        var img=hb[index];
        if(!img){return;}
        wx.showLoading({
          title: '保存中...', 
          mask: true,
        });
        wx.downloadFile({
          url:img,
          success: function(res) {
            if (res.statusCode === 200) {
              let img = res.tempFilePath;
              wx.saveImageToPhotosAlbum({
                filePath: img,
                success(res) {
                app.msg('海报保存成功');
              },
                fail(res) {
app.msg('海报保存失败，如果您没有设置保存权限，请点击胶囊按钮进行授权。');
                }
              });
            }
          }
        });
      },

  onShareAppMessage: function () {//分享
    var shop=this.data.shop;//门店
    var img='';var title=shop.shop_name;
    if(shop.data){
   img=shop.data.share_img;
if(shop.data.share_title){title=shop.data.share_title;}
    }
if(!title){title=app.globalData.app_name}
if(!img){//如果没有设置分享图片，则获取首页广告图片
  var adv=this.data.adv;
  if(adv['main_swiper']){
    var ad=adv.main_swiper[0].adv.item;
    var im=[]
    for(var i in ad){
im.push(ad[i].val)
    }
    var num=Object.keys(ad).length;
    var rand=fun.rand(num)-1;
    if(rand<0){rand=0;}
    img=im[rand];
  }
}
if(!img){img='../img/logo.jpg';}
//用户
    var user_id=app.globalData.user_id;if(!user_id){user_id='';}
    var url='/pages/index/index?id='+shop.id+'&f='+user_id;
  console.log('分享',img,url)
this.setData({share_box_show:''});//分享按钮窗口关闭
    return {
      title:title,
      path: url,
      imageUrl: img,
    }
  }, onShareTimeline: function () { 
    var shop=this.data.shop;//门店
    var img='';var title=shop.shop_name;
    if(shop.data){
   img=shop.data.share_img;
if(shop.data.share_title){title=shop.data.share_title;}
    }
if(!title){title=app.globalData.app_name}
if(!img){//如果没有设置分享图片，则获取首页广告图片
  var adv=this.data.adv;
  if(adv['main_swiper']){
    var ad=adv.main_swiper[0].adv.item;
    var im=[]
    for(var i in ad){
im.push(ad[i].val)
    }
    var num=Object.keys(ad).length;
    var rand=fun.rand(num)-1;
    if(rand<0){rand=0;}
    img=im[rand];
  }
}
if(!img){img='../img/logo.jpg';}
//用户
    var user_id=app.globalData.user_id;if(!user_id){user_id='';}
    var url='/pages/index/index?id='+shop.id+'&f='+user_id;
  console.log('分享',img,url)
this.setData({share_box_show:''});//分享按钮窗口关闭
    return {
      title:title,
      path: url,
      imageUrl: img,
    }
  },go(e){ 
    var url=e.currentTarget.dataset.url;
    if(!url){app.msg('没有设置链接，请到后台设置或者联系我们');return;}
    wx.navigateTo({url: e.currentTarget.dataset.url})},
  back(e){wx.navigateBack({delta:0})},
  home(e){ wx.reLaunch({url: '/pages/index/index'})},
  rgo(e){wx.redirectTo({url: e.currentTarget.dataset.url})},
  login: function (e) { wx.navigateTo({url: '/pages/login/index'})},
  win_open(e){var c=e.currentTarget.dataset.c;this.setData({[c]:'show'})},//打开窗口
  win_close(e){var c=e.currentTarget.dataset.c;this.setData({[c]:''})},//关闭窗口
open_min(e){wx.navigateToMiniProgram({appId: e.currentTarget.dataset.url,path: '/pages/index/index',})},
scan(e){fun.scan();},
})
