
var app = getApp()
var fun = require('../fun.js');
Page({
  data: {
    nav:{top:app.globalData.nav_top,title:app.globalData.web,home:'show',back:'show',safe_top:app.globalData.safe_top,bottom:app.globalData.safe_bottom},page:1,can_load:1,z_top:-20,can_scroll:false,duo:app.globalData.duo,share_bt:'1'
  },

  onLoad: function (op) {
  },

  onReady: function () {

    this.load()
  },

  onShow: function () {

  },
load(){//导入菜肴
  var shop_id=app.globalData.shop_id;

//console.log(shop_id)
var key=this.data.key;if(!key){key='';}
var page=this.data.page;if(!page){page=1;}
var can_load=this.data.can_load;if(can_load!=1){return;}
this.data.can_load=0;
var desk_id=app.globalData.desk_id;if(!desk_id){desk_id=0;}
fun.get({ac:'get_pro_list',key:key,page:page,shop_id:shop_id,path:'rest',desk_id:desk_id}).then(res=>{
if(page==1){
this.setData({
list:res.list?res.list:'',
user:res.user,
can_load:1,
h:res.h,
style:res.style,
quan:res.quan,
info:res.info,
quan_show:res.quan_show,
app:res.app,
shop:res.shop,
my_quan:res.my_quan,//我的优惠券
zeng:res.zeng,//满赠
desk:res.desk
});
var app=res.app;
if(app){
//登录
if(app.data.login_first==1 && !res.user){
this.login();
}
}
//获取缓存
var cart_temp= wx.getStorageSync('cart_'+shop_id);
if(cart_temp){
  var cart=JSON.parse(cart_temp);
  this.setData({cart:cart});
  this.format_cart()
}


}else{//更多数据
var l=res.list;
if(!l){return;}
var list=this.data.list;
for(var i in l){
  list.push(l[i])
}
this.setData({list:list,can_load:1})
}
})

},
load_more(){//获取更多
var can_load=this.data.can_load;if(can_load!=1){return}
var page=this.data.page;if(!page){page=1;}
page++;
this.data.page=page;
this.load()
},
  onHide: function () {

  },
quan_get(){//获取优惠券
  var shop=this.data.shop;
  fun.get({ac:'get_quan',shop_id:shop.id}).then(res=>{
if(res.err=='no_login'){return;}
this.setData({quan_show:'',my_quan:res.list});
app.msg('优惠券领取成功');
  })
},
  onPageScroll(e){
if(e.scrollTop>=180){
this.setData({can_scroll:true})
}
if(e.scrollTop<=0){
this.setData({can_scroll:false})
}

  },main_scroll(e){
var top=e.detail.scrollTop;

var h=this.data.h;
for(var i in h){
var min=parseInt(h[i].min);
var max=parseInt(h[i].max);
var is_select=parseInt(h[i].is_select);
if(top>min && top<max){//在此区间
  //h[i]['is_select']=1;
  var temp={};
  var zz_top=0;var z_top=0;
  if(is_select!=1){
    for(var j in h){
      if(j==i){ h[j]['is_select']=1;
    z_top=(zz_top)*200-20;
    }else{ h[j]['is_select']=0;}
    temp[j]=h[j];zz_top++;
    }
    temp[i]['is_select']=1;
   // console.log('保存',i,top,min,max,z_top)
    this.setData({h:temp,z_top:z_top});
  }
}

}


  },get_c(e){//转到
var c=e.currentTarget.dataset.index;
var h=this.data.h;
var min=parseInt(h[c].min);
//console.log(c,min);
var zz_top=0;var z_top=0;
for(var i in h){
if(i==c){h[i]['is_select']=1;z_top=(zz_top)*200-20;}else{h[i]['is_select']=0;}
zz_top++;
}
var can_scroll=this.data.can_scroll;var that=this;
if(can_scroll==false){this.setData({can_scroll:'true'});
setTimeout(function(){that.setData({can_scroll:false})},600)}
this.setData({scroll_top:min+5,h:h,z_top:z_top});
//如果主页面每天到顶，则可以到顶
  },
  get_item_total(items){//内部的总数量
    let total=0;let buy=0;
    for(var i in items){
    var price=parseFloat(items[i].price);
    var num=parseInt(items[i].buy);
    var t=price*num;
    items[i]['total']=fun.number_fromat(t,2);
    items[i]['price']=fun.number_fromat(price,2);
    buy+=num;
    total+=t;
    }
    var re={item:items,total:total,buy:buy}
return re;
  },

  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },go(e){ wx.navigateTo({url: e.currentTarget.dataset.url})},
  back(e){wx.navigateBack({delta:0})},
  home(e){ wx.reLaunch({url: '/pages/index/index'})},
  rgo(e){wx.redirectTo({url: e.currentTarget.dataset.url})},
  login: function (e) {wx.navigateTo({url: '/pages/login/index',})},
  win_open(e){var c=e.currentTarget.dataset.c;this.setData({[c]:'show'})},//打开窗口
  win_close(e){var c=e.currentTarget.dataset.c;this.setData({[c]:''})},//关闭窗口
  open_min(e){wx.navigateToMiniProgram({appId: e.currentTarget.dataset.url,path: '/pages/index/index',})},

add_cart: function (e) {
  
    },clean_cart(e){//清除购物车
      var that=this;
wx.showModal({
  title: '警告',content:'是否删除所有购物车商品?',success(res){
    if(res.confirm){
      that.format_cart('clean')
    }
  }
})

    },
    format_cart(ac){
    },
    show_cart: function (e) {
    },
select_this_cart(e){
  },
format_cart_info(cart_info){
},

  cart_num_add(e){
  },cart_add(e){
  },cart_save(){
  },cart_num_change(e){
  },
  buy(e){
  },scan(e){fun.scan();},get_hb(){//分享海报
    var shop=this.data.shop;
    var shop_id=app.globalData.shop_id;
    if(shop){shop_id=shop.id}
    fun.get({ac:'get_shop_hb',id:shop_id,path:'hb'}).then(res=>{
    this.setData({hb:res.hb,hb_show:'show',hb_index:0,share_box_show:''});
    })
    },hb_change(e){//切换海报后的index
      //console.log(e)
    var val=e.detail.current;
    //console.log(val)
    this.data.hb_index=val;
    },
    hb_save(e){//保存图片
      console.log('保存海报')
            var that=this;
            var index=this.data.hb_index
            var hb=this.data.hb;
            if(!hb){app.err('没有海报信息');return;}
            var img=hb[index];
            console.log(img)
            if(!img){return;}
            wx.showLoading({
              title: '保存中...', 
              mask: true,
            });
            wx.downloadFile({
              url:img,
              success: function(res) {
                if (res.statusCode === 200) {
                  let img = res.tempFilePath;
                  wx.saveImageToPhotosAlbum({
                    filePath: img,
                    success(res) {
                    app.msg('海报保存成功');
                  },
                    fail(res) {
    app.msg('海报保存失败，如果您没有设置保存权限，请点击胶囊按钮进行授权。');
                    }
                  });
                }
              }
            });
          }, onShareAppMessage: function () {//分享
            var shop=this.data.shop;//门店
            var img;var title=shop.shop_name;
           
            if(shop.data){
           img=shop.data.share_img;
        if(shop.data.share_title){title=shop.data.share_title;}
            }
        if(!title){title=app.globalData.app_name}
        if(!img){//如果没有设置分享图片，则获取首页广告图片
         
          var adv=shop.adv; 
          if(adv['item']){
            var ad=adv.item;
            var im=[]
            for(var i in ad){
        im.push(ad[i].val)
            }
            var num=Object.keys(ad).length;
            var rand=fun.rand(num)-1;
            if(rand<0){rand=0;}
            img=im[rand];
          }
        }
        if(!img){img='../img/logo.jpg';}
        //用户
            var user_id=app.globalData.user_id;if(!user_id){user_id='';}
            var url='/pages/index/index?id='+shop.id+'&f='+user_id;
          console.log('分享',img,url)
        this.setData({share_box_show:''});//分享按钮窗口关闭
            return {
              title:title,
              path: url,
              imageUrl: img,
            }
          }, onShareTimeline: function () { 
            var shop=this.data.shop;//门店
            var img;var title=shop.shop_name;
           
            if(shop.data){
           img=shop.data.share_img;
        if(shop.data.share_title){title=shop.data.share_title;}
            }
        if(!title){title=app.globalData.app_name}
        if(!img){//如果没有设置分享图片，则获取首页广告图片
         
          var adv=shop.adv; 
          if(adv['item']){
            var ad=adv.item;
            var im=[]
            for(var i in ad){
        im.push(ad[i].val)
            }
            var num=Object.keys(ad).length;
            var rand=fun.rand(num)-1;
            if(rand<0){rand=0;}
            img=im[rand];
          }
        }
        if(!img){img='../img/logo.jpg';}
        //用户
            var user_id=app.globalData.user_id;if(!user_id){user_id='';}
            var url='/pages/index/index?id='+shop.id+'&f='+user_id;
          console.log('分享',img,url)
        this.setData({share_box_show:''});//分享按钮窗口关闭
            return {
              title:title,
              path: url,
              imageUrl: img,
            }
          }
})
