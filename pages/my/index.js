
var app = getApp()
var fun = require('../fun.js');
Page({
  data: {
    nav:{top:app.globalData.nav_top,title:'个人中心',home:'show',back:'show',safe_top:app.globalData.safe_top,page:'my'},page:1,can_load:1,
  },

  onLoad: function (options) {

  },

  onReady: function () {
    if(getCurrentPages().length==1){var nav=this.data.nav;nav.back='';this.setData({nav:nav})}
this.load();
  },

  onShow: function () {
if(app.globalData.flash==1){this.load()}
app.globalData.flash=''
  },


  load: function () {
    var shop=app.globalData.shop_id;
    var shop_id=0;
    if(shop){shop_id=shop;}
fun.get({ac:'my',shop:shop_id}).then(res=>{
  if(res.err=='no_login'){return;}
  this.setData({list:res.list,user:res.user,icon:res.icon,adv:res.adv,info:res.info,app:res.app})
})
  },

  onUnload: function () {

  },


  loginout: function () {//注销
var that=this;
wx.showModal({
  title: '提示',content:'是否退出登录?',success(res){
    if(res.confirm){
      try {
        wx.setStorageSync('login_info', '')
      } catch (e) { }
app.globalData.adm_id='';
app.globalData.user_id='';
that.load()
    }
  }
})
  },
  hexiao(e){//核销订单
    var that=this;
wx.scanCode({
  success(res){
    console.log(res);
fun.get({ac:'hexiao',val:res.result}).then(res=>{
  if(res.err=='no_login'){return;}

})
  }
})
  },
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },go(e){ wx.navigateTo({url: e.currentTarget.dataset.url})},
  back(e){wx.navigateBack({delta:0})},
  home(e){ wx.reLaunch({url: '/pages/index/index'})},
  rgo(e){wx.redirectTo({url: e.currentTarget.dataset.url})},
  login: function (e) {wx.navigateTo({url: '/pages/login/index',})},
  win_open(e){var c=e.currentTarget.dataset.c;this.setData({[c]:'show'})},//打开窗口
  win_close(e){var c=e.currentTarget.dataset.c;this.setData({[c]:''})},//关闭窗口
open_min(e){wx.navigateToMiniProgram({appId: e.currentTarget.dataset.appid,path: e.currentTarget.dataset.url,})},
scan(e){fun.scan();},
})
