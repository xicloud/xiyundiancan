
function indexOf(str, v) {
  if (str.indexOf(v) < 0) { return false; } else { return true; }
}

function get_pos(){
  
}

function get(data){
  var app = getApp();
  if(!data){data={};}
  data.user_id=app.globalData.user_id;if(!data.user_id){data.user_id=0;}
  data.session=app.globalData.session;if(!data.session){data.session='';}
  data.adm_id=app.globalData.adm_id;if(!data.adm_id){data.adm_id=0;}
  data.adm_session=app.globalData.adm_session;if(!data.adm_session){data.adm_session='';}
  data.from_id=app.globalData.from_id;if(!data.from_id){data.from_id=0;}
  data.user_session=app.globalData.user_session;
  data.from=app.globalData.from;if(!data.from){data.from='';}
  if(!data.path){data.path='';}
  data.app_id=app.globalData.app_id;if(!data.app_id){data.app_id=0}
  if(!data.shop_id){
data.shop_id=app.globalData.shop_id;if(!data.shop_id){data.shop_id=0;}//门店
}
//data.duo=app.globalData.duo;//多门店版本
  data.adm=app.globalData.adm;if(!data.adm){data.adm=0;}//管理员
  data.latitude=app.globalData.latitude;if(!data.latitude){data.latitude=0}
  data.longitude=app.globalData.longitude;if(!data.longitude){data.longitude=0}
  console.log(data)
  return new Promise((resolve, reject) => {
wx.request({
  url: app.globalData.server+data.path+'/server.php',
  header:{'content-type':'application/x-www-form-urlencoded'},
  method:'POST',
  enableHttp2:true,
  data:data,success(res){
if(!res.data){console.log('没有返回数据');return;}console.log(res.data)
if(res.data.err!='ok'){ 
  if(typeof res.data=='string'){
if(res.data.indexOf('404 Not Found')!=-1){app.err('服务器页面丢失，请联系管理员处理');return;} 
}
if(res.data.direct && res.data.url){wx.navigateTo({url: '/pages/login/index',});resolve({err:'no_login'});return;}//跳转页面
if(res.data.err=='no_login'){wx.navigateTo({url: '/pages/login/index',});resolve({err:'no_login'});return;}
if(res.data.err=='adm_login'){wx.navigateTo({url: '/pages/login/adm_login',});resolve({err:'no_login'});return;}
if(res.data.err!=''){app.err(res.data.err);return;}
} 
if(res.data.err_msg){
  app.msg(res.data.err_msg)
}
    resolve(res.data)
  },fail(res){
    console.log('错误',res)
  }
})
  });
}

function get_phone(data) {//获取手机号码
  var app = getApp()
  return new Promise((resolve, reject) => {
    wx.login({
      success(res) {
        console.log('获取手机号码',res,data);
            wx.request({
              url: app.globalData.server+'server.php', // 目标服务器url
              data: {
                ac: 'get_phone',
                header:{'content-type':'application/x-www-form-urlencoded'},
                method:'POST',
                encryptedData:encodeURIComponent(data.encryptedData) ,
                iv: data.iv,
                code: res.code,
                adm:app.globalData.adm?app.globalData.adm:0
              },
              success: (res) => {console.log(res.data)
                if (!res.data.err_code) {
                  resolve(res.data)
                } else {app.msg('获取失败，请重新获取');
                  console.log('获取失败',res.data);
                }
              }
            }); 
          },

      fail(res) {
        console.log(`login调用失败`);
      }
    });
  });
}

function go_login() {
  var app = getApp()
  return new Promise((resolve, reject) => {
    wx.getUserProfile({
      desc: '微信登录',
      success: res => {
       app.globalData.userInfo=res.userInfo;
       wx.login({
        success(res) {
          console.log('login code',res);
          var shop=app.globalData.shop;
          var shop_id=app.globalData.shop_id;
          if(shop){shop_id=shop.id;}
          if(!shop_id){shop_id=0;}
              wx.request({
                url: app.globalData.server+'server.php', // 目标服务器url
                header:{'content-type':'application/x-www-form-urlencoded'},
                method:'POST',
                data: {
                  ac: 'wx_login',
                  nickName: app.globalData.userInfo.nickName,
                  logo: app.globalData.userInfo.avatarUrl,
                  city: app.globalData.userInfo.city,
                  province: app.globalData.userInfo.province,
                  encryptedData: res.encryptedData,
                  iv: res.iv,
                  sign: res.signature, 
                  code: res.code,
                  from_id: app.globalData.from_id ? app.globalData.from_id : 0,
                  user_session:app.globalData.user_session?app.globalData.user_session:0,
                  adm:app.globalData.adm?app.globalData.adm:0,
                  app_id:app.globalData.app_id?app.globalData.app_id:0,
                  shop_id:shop_id,//门店
                },
                success: (res) => {console.log(res.data)
                  if (res.data.err == 'ok') {
                    app.globalData.user_id = res.data.id;
                    app.globalData.session = res.data.session;
                    app.globalData.user_info = res.data;
                    res.data.user_id = res.data.id;
                    console.log('登录成功')
                    try {
                      wx.setStorageSync('login_info', JSON.stringify(res.data))
                    } catch (e) { }
                    resolve('ok')
                  } else {
                    console.log('登录错误',res.data);
                    app.msg('登录错误:'+res.data.err);
                  }
                }
              }); 
            },
  
        fail(res) {
          console.log(`login调用失败`);
        }
      });

      }
    })
  });
}



function get_time(start,end){
var time=end-start;if(time<0){return 0;}
var h='';
var day=Math.floor(time/(3600*24));
if(day){h+=day+'天';time=time-day*3600*24;}
var hour=Math.floor(time/3600);
if(hour){h+=hour+'时';time=time-hour*3600;}
var minit=Math.floor(time/60);
if(minit){h+=minit+'分';time=time-minit*60;}
h+=time+'秒';
return h;
}

function number_fromat(num, n) {
  var nn = parseFloat(num);
  return nn.toFixed(n);
}

function get_dis(){}

function q(id){ return new Promise((resolve, reject) => {
const query = wx.createSelectorQuery();
query.select(id).boundingClientRect();
query.selectViewport().scrollOffset();
query.exec(function(res){
  resolve(res)
});
});
}

function is_phone(num){
  let reg = /^1(3[0-9]|4[5,7]|5[0,1,2,3,5,6,7,8,9]|6[2,5,6,7]|7[0,1,7,8]|8[0-9]|9[1,8,9])\d{8}$/;
  return reg.test(num);
}

function scan(){
}

function rand(max){
 return Math.floor(Math.random() * max)
}

module.exports = { indexOf: indexOf, number_fromat: number_fromat, go_login: go_login,get_dis:get_dis,get:get,q:q,get_time:get_time,is_phone:is_phone,get_phone:get_phone,get_pos:get_pos,scan:scan,rand:rand }
